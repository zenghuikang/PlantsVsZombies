package com.shemy.plantsvszombies;

/**
 * Created by 46950 on 2018/11/14.
 */

public class Chomper extends Plant {
    public Chomper() {
        super("plant/Chomper/Frame%02d.png", 13);
        setPrice(150);
    }
}
