package com.shemy.plantsvszombies;

import android.view.MotionEvent;

import org.cocos2d.layers.CCLayer;
import org.cocos2d.layers.CCScene;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.transitions.CCFadeTransition;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGRect;

class HelpLayer extends CCLayer {

    private CCSprite ccSprite_back;

    public HelpLayer(){
        init();
    }

    public void init(){
        setIsTouchEnabled(true);
        CCSprite ccSprite_bg=CCSprite.sprite("help/help_bg.png");
        ccSprite_bg.setAnchorPoint(0,0);
        ccSprite_bg.setScale(3.5);
        addChild(ccSprite_bg);


        CCSprite ccSprite_help=CCSprite.sprite("help/help.png");
        ccSprite_help.setPosition(175,125);
        ccSprite_help.setScale(0.4);
        ccSprite_bg.addChild(ccSprite_help);

        ccSprite_back=CCSprite.sprite("dialog/back_menu.png");
        ccSprite_back.setScale(2);
        ccSprite_back.setPosition(600,100);
        addChild(ccSprite_back);
    }

    @Override
    public boolean ccTouchesBegan(MotionEvent event) {
        CGPoint cgPoint = convertTouchToNodeSpace(event);
        if (CGRect.containsPoint(ccSprite_back.getBoundingBox(),cgPoint)){
            CCScene ccScene = CCScene.node();
            ccScene.addChild(new MenuLayer());
            CCFadeTransition ccFadeTransition = CCFadeTransition.transition(2,ccScene);
            CCDirector.sharedDirector().runWithScene(ccFadeTransition);
        }
        return super.ccTouchesBegan(event);
    }
}
