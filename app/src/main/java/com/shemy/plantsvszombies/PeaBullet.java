package com.shemy.plantsvszombies;

import org.cocos2d.actions.instant.CCHide;
import org.cocos2d.actions.interval.CCDelayTime;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.nodes.CCSprite;

/**
 * Created by Dzsom on 2018/11/27.
 */

public class PeaBullet extends Bullet {
    public PeaBullet( ShootPlant shootPlant) {
        super( "bullet/bullet1.png",shootPlant);
    }

    @Override
    public void showBulletBlast(Zombie zombie) {
        CCSprite ccSprite_bulletBlast=CCSprite.sprite("bullet/bulletBlast1.png");
        ccSprite_bulletBlast.setPosition(ccp(zombie.getPosition().x,zombie.getPosition().y+60));
        getParent().addChild(ccSprite_bulletBlast,6);
        CCDelayTime ccDelayTime = CCDelayTime.action(0.1f);
        CCHide ccHide = CCHide.action();
        CCSequence ccSequence =CCSequence.actions(ccDelayTime,ccHide);
        ccSprite_bulletBlast.runAction(ccSequence);
    }

    @Override
    public void showBulletBlast(ZombieImp zombieImp) {
        CCSprite ccSprite_bulletBlast=CCSprite.sprite("bullet/bulletBlast1.png");
        ccSprite_bulletBlast.setPosition(ccp(zombieImp.getPosition().x,zombieImp.getPosition().y+60));
        getParent().addChild(ccSprite_bulletBlast,6);
        CCDelayTime ccDelayTime = CCDelayTime.action(0.1f);
        CCHide ccHide = CCHide.action();
        CCSequence ccSequence =CCSequence.actions(ccDelayTime,ccHide);
        ccSprite_bulletBlast.runAction(ccSequence);
    }
}
